import networkx as nx
import networkx.algorithms.traversal as tx
import matplotlib.pyplot as plot
#g = nx.Graph()
#g.add_edge('a', 'b', {'name': 'ferhat'}, weight=0.1)
#g.add_edge('b', 'c', weight=1.5)
#g.add_edge('a', 'c', weight=1.0)
#g.add_edge('c', 'd', weight=2.2)
#
#print g.nodes(data=True)
#print g.edges()
#
#print nx.shortest_path(g, 'b', 'd')
#
#t = nx.Graph()
#t.add_path([0, 1, 2, 3])
#t.add_nodes_from([10, 11, 12, 13])
#t.add_edges_from([(10, 11), (11, 10), (11, 12)])
#
#print t.nodes()
#print t.edges()

G = nx.Graph()
G.add_path(range(10))
# G.add_edges_from([(1, 5), (7, 9), (7, 7)])
print 'NODES : ', G.nodes()
print 'EDGES : ', G.edges()

G.has_edge(1, 5)
G.has_node(7)

target = 9
print 'neighbors of',target, G.neighbors(target)

print 'Edges of', target
for x in tx.bfs_edges(G, target):
    print x,
print
print 'Successors of', target
for x in tx.bfs_successors(G, target):
    print x,
print
print 'Predecessors of', target
for x in tx.bfs_predecessors(G, target):
    print x,

print

print 'len', len (G.edges())

nx.draw(G)
plot.show()