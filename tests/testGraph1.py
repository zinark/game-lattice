import networkx as nx
import matplotlib.pyplot as plot

def test():
    class Node(object):
        def __init__(self, name):
            self.name = name

    def __repr__(self):
        return self.name

    g = nx.Graph()

    a = Node('A')
    b = Node('B')
    c = Node('C')
    d = Node('D')
    e = Node('E')

    nodes = [a, b, c, d, e]
    for node in nodes:
        g.add_node(node, time='5pm')

    g.add_edge(a, b, color='blue')
    g.add_edge(c, e, color='blue')
    g.add_edge(e, d)

    g.add_path(range(5))
    g.add_path(range(9, 18))
    print g.degree(0)
    print g.node[a]

    print g[a]
    print g[b]
    print g[c]
    print g[d]

    p = nx.complete_graph(6)
    p = nx.petersen_graph()
    p = nx.barbell_graph(10, 10)
    p = nx.lollipop_graph(10, 10)
    p = nx.erdos_renyi_graph(15, 0.15)
    # p = nx.random_lobster(40, 0.9, 0.9)
    p = nx.watts_strogatz_graph(30, 2, 0.1) # My favorite

    nx.draw_networkx_edge_labels(g, nx.spring_layout(g))
    plot.pyplot.show()

def composeTest ():
    g1 = nx.path_graph(10)
    g2 = nx.path_graph(5)
    result = nx.compose (g1, g2)
    print result.nodes ()
    print result.edges ()

def diameter ():
    g = nx.path_graph(10)
    g = g.subgraph([1,2,3])
    print g.nodes ()
    print g.edges ()
    result = nx.diameter(g)
    print result

diameter()