rem WIN32 environment
virtualenv lattice
cd lattice
./scripts/activate
git clone https://bitbucket.org/zinark/game-lattice
pip install -r ./game-lattice/requirements.txt
wget http://sourceforge.net/projects/matplotlib/files/matplotlib/matplotlib-1.1.1/matplotlib-1.1.1.win32-py2.7.exe/download
wget http://pypi.python.org/packages/2.7/n/numpy/numpy-1.6.2.win32-py2.7.exe

easy_install ./numpy-1.6.2.win32-py2.7.exe
easy_install ./matplotlib-1.1.1.win32-py2.7.exe
mv ./Lib/site-packages/numpy-1.6.2-py2.7-win32.egg/numpy ./Lib/site-packages/
mv ./Lib/site-packages/matplotlib-1.1.1-py2.7-win32.egg/matplotlib ./Lib/site-packages/