app_version = {
    'major': 0,
    'minor': 1,
    'counter': 1,
    }


def getAppVersion():
    return '{0}.{1}.{2}'.format(
        app_version['major'],
        app_version['minor'],
        app_version['counter'],
        )

__version__ = getAppVersion()

print 'Lattice Version : ' + str(getAppVersion())