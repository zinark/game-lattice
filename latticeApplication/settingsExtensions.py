import os

def localPath(url):
    currentDir = os.path.dirname(__file__)
    absPath = os.path.abspath(currentDir)
    return os.path.join(absPath, url)